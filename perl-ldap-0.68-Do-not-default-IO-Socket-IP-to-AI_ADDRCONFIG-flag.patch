From 957a39309fa2f8a85a6b8a0a2c5b1751a151cb8b Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Petr=20P=C3=ADsa=C5=99?= <ppisar@redhat.com>
Date: Tue, 16 Feb 2021 15:29:58 +0100
Subject: [PATCH] Do not default IO::Socket::IP to AI_ADDRCONFIG flag
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

t/40connect.t fails if the only available network interface is
loopback and IO::Socket::IP is installed:

    # perl -Ilib -I. t/40connect.t
    1..3
    ok 1 - client with IPv4/IPv6 auto-selection, bound to ::1
    ldap://localhost:9009/ Name or service not known at t/common.pl line 157.
    # Looks like your test exited with 22 just after 1.

The reason is that IO::Socket::IP by default resolves host names with
AI_ADDRCONFIG flag and in the particular case (no interfaces other
than loopback) a system resolver (glibc in my case) hides both IPv4
and IPv6 addreses of the hostname (e.g. localhost).

See <https://rt.cpan.org/Ticket/Display.html?id=104793> for more
details.

I applied a workaround similar to one found in IO-Socket-SSL.
I believe that other Socket implementations perl-ldap can use do not
suffer from this problem.

Signed-off-by: Petr Písař <ppisar@redhat.com>
---
 lib/Net/LDAP.pm | 3 +++
 1 file changed, 3 insertions(+)

diff --git a/lib/Net/LDAP.pm b/lib/Net/LDAP.pm
index 5dfe3e3..be11d12 100644
--- a/lib/Net/LDAP.pm
+++ b/lib/Net/LDAP.pm
@@ -167,6 +167,9 @@ sub connect_ldap {
     LocalAddr  => $arg->{localaddr} || undef,
     Proto      => 'tcp',
     ($class eq 'IO::Socket::IP' ? 'Family' : 'Domain')     => $domain,
+    # Work around IO::Socket::IP defaulting to AI_ADDRCONFIG which breaks
+    # resolution if only a loopback interface is available. CPAN RT#104793.
+    ($class eq 'IO::Socket::IP' and $domain ne AF_UNSPEC ? ('GetAddrInfoFlags' => 0) : ()),
     MultiHomed => $arg->{multihomed},
     Timeout    => defined $arg->{timeout}
 		 ? $arg->{timeout}
-- 
2.26.2

